// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright (c) 2022 Th3-S1lenc3

use derive_setters::*;

// IDEA: Add documentation for this module

#[derive(Debug, Default, Setters, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
#[setters(borrow_self, prefix = "set_")]
pub struct Connector {
    pub name: String,                        // HDMI 1
    pub status: String,                      // (Dis)connected
    pub primary: bool,                       // True if primary
    pub current_resolution: Resolution,      // 1920x1080,
    pub current_refresh_rate: String,        // 60
    pub prefered_resolution: Resolution,     // 2560×1440
    pub prefered_refresh_rate: String,       // 120
    pub position: Position,                  // 0,0 or 1920,0 or ...
    pub orientation: String,                 // normal, left, etc...
    pub available_orientations: Vec<String>, // normal, left, etc...
    pub physical_dimensions: Dimensions,     // 1210mm x 680mm
    pub output_info: Vec<Output>,
}

#[derive(Debug, Default, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
pub struct Position {
    pub x: String, // 0 or 1920 or ...
    pub y: String, // 0 or 1080 or ...
}

#[derive(Debug, Default, PartialEq, Clone, serde::Serialize, serde::Deserialize)]
pub struct Dimensions {
    pub x: String, // 1210 or ...
    pub y: String, // 680 or ...
}

#[derive(Debug, Default, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Resolution {
    pub horizontal: String, // 1920
    pub vertical: String,   // 1080
}

impl Resolution {
    pub fn pretty(&self) -> String {
        format!("{}x{}", self.horizontal, self.vertical)
    }
}

#[derive(Debug, Default, Clone, PartialEq, serde::Serialize, serde::Deserialize)]
pub struct Output {
    pub resolution: Resolution,
    pub rates: Vec<String>,
}

impl Connector {
    /// Create a new instance of Connector
    pub fn new() -> Self {
        Connector::default()
    }

    /// Getter function for `Connector.name`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let name = &connector.name();
    ///
    ///     println!("Connector name: {}", name);
    ///
    /// #   assert_eq!(name, &"HDMI-1".to_string());
    ///     Ok(())
    /// }
    /// ```
    pub fn name(&self) -> String {
        self.name.clone()
    }

    /// Getter function for `Connector.status`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let status = &connector.status();
    ///
    ///     println!("Connector status: {}", status);
    ///
    /// #   assert_eq!(status, &"connected".to_string());
    ///     Ok(())
    /// }
    /// ```
    pub fn status(&self) -> String {
        self.status.clone()
    }

    /// Getter function for `Connector.primary`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let primary = &connector.primary();
    ///
    ///     println!("Connector primary: {}", primary);
    ///
    /// #   assert_eq!(primary, &true);
    ///     Ok(())
    /// }
    /// ```
    pub fn primary(&self) -> bool {
        self.primary.clone()
    }

    /// Getter function for `Connector.current_resolution`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    /// # use xrandr_parser::connector::Resolution;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let current_resolution = &connector.current_resolution();
    ///
    ///     println!("Connector Current Resolution: {:#?}", current_resolution);
    ///
    /// #   assert_eq!(current_resolution, &Resolution {
    /// #       horizontal: "1920".to_string(),
    /// #       vertical: "1080".to_string(),
    /// #   });
    ///     Ok(())
    /// }
    /// ```
    pub fn current_resolution(&self) -> Resolution {
        self.current_resolution.clone()
    }

    /// Getter function for `Connector.current_refresh_rate`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let current_refresh_rate = &connector.current_refresh_rate();
    ///
    ///     println!("Connector Current Refresh Rate: {}", current_refresh_rate);
    ///
    /// #   assert_eq!(current_refresh_rate, &"60.00".to_string());
    ///     Ok(())
    /// }
    /// ```
    pub fn current_refresh_rate(&self) -> String {
        self.current_refresh_rate.clone()
    }

    /// Getter function for `Connector.prefered_resolution`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    /// # use xrandr_parser::connector::Resolution;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let prefered_resolution = &connector.prefered_resolution();
    ///
    ///     println!("Connector Prefered Resolution: {:#?}", prefered_resolution);
    ///
    /// #   assert_eq!(prefered_resolution, &Resolution {
    /// #       horizontal: "1920".to_string(),
    /// #       vertical: "1080".to_string(),
    /// #   });
    ///     Ok(())
    /// }
    /// ```
    pub fn prefered_resolution(&self) -> Resolution {
        self.prefered_resolution.clone()
    }

    /// Getter function for `Connector.prefered_refresh_rate`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let prefered_refresh_rate = &connector.prefered_refresh_rate();
    ///
    ///     println!("Connector Prefered Refresh Rate: {}", prefered_refresh_rate);
    ///
    /// #   assert_eq!(prefered_refresh_rate, &"60.00".to_string());
    ///     Ok(())
    /// }
    /// ```
    pub fn prefered_refresh_rate(&self) -> String {
        self.prefered_refresh_rate.clone()
    }

    /// Getter function for `Connector.primary`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    /// # use xrandr_parser::connector::Position;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let position = &connector.position();
    ///
    ///     println!("Connector position: {:#?}", position);
    ///
    /// #   assert_eq!(position, &Position {
    /// #       x: "0".to_string(),
    /// #       y: "0".to_string()
    /// #   });
    ///     Ok(())
    /// }
    /// ```
    pub fn position(&self) -> Position {
        self.position.clone()
    }

    /// Getter function for `Connector.orientation`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let orientation = &connector.orientation();
    ///
    ///     println!("Connector orientation: {}", orientation);
    ///
    /// #   assert_eq!(orientation, &"normal".to_string());
    ///     Ok(())
    /// }
    /// ```
    pub fn orientation(&self) -> String {
        self.orientation.clone()
    }

    /// Getter function for `Connector.available_orientations`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let available_orientations = &connector.available_orientations();
    ///
    ///     println!("Connector Available Orientations: {:#?}", available_orientations);
    ///
    /// #   assert_eq!(available_orientations, &vec![
    /// #       "normal".to_string(),
    /// #       "left".to_string(),
    /// #       "inverted".to_string(),
    /// #       "right".to_string(),
    /// #       "x axis".to_string(),
    /// #       "y axis".to_string(),
    /// #   ]);
    ///     Ok(())
    /// }
    /// ```
    pub fn available_orientations(&self) -> Vec<String> {
        self.available_orientations.clone()
    }

    /// Getter function for `Connector.physical_dimensions`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    /// # use xrandr_parser::connector::Dimensions;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let physical_dimensions = &connector.physical_dimensions();
    ///
    ///     println!("Connector Physical Dimensions: {:#?}", physical_dimensions);
    ///
    /// #   assert_eq!(physical_dimensions, &Dimensions {
    /// #       x: "1210".to_string(),
    /// #       y: "680".to_string(),
    /// #   });
    ///     Ok(())
    /// }
    /// ```
    pub fn physical_dimensions(&self) -> Dimensions {
        self.physical_dimensions.clone()
    }

    /// Getter function for `Connector.output_info`
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    /// # use xrandr_parser::connector::{Output, Resolution};
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let output_info = &connector.output_info();
    ///
    ///     println!("Connector Output Info: {:#?}", output_info);
    ///
    /// #   assert_eq!(output_info, &vec![
    /// #       Output {
    /// #           resolution: Resolution {
    /// #               horizontal: "1920".to_string(),
    /// #               vertical: "1080".to_string(),
    /// #           },
    /// #           rates: vec![
    /// #               "60.00".to_string(),
    /// #           ].to_vec(),
    /// #       }
    /// #   ]);
    ///     Ok(())
    /// }
    /// ```
    pub fn output_info(&self) -> Vec<Output> {
        self.output_info.clone()
    }

    /// Get available resolutions for a specific connector in `struct` form. If the resolution
    /// is not found it returns an empty `Vec<Resolution>` not an error.
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    /// # use xrandr_parser::connector::*;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let available_resolutions = &connector.available_resolutions()?;
    ///
    ///     println!("Available Resolutions: {:#?}", available_resolutions);
    ///
    /// # assert_eq!(available_resolutions, &[
    /// #     Resolution {
    /// #         horizontal: "1920".to_string(),
    /// #         vertical: "1080".to_string(),
    /// #     }
    /// # ].to_vec());
    ///     Ok(())
    /// }
    /// ```
    // TODO: Condense these two functions into a single generic function.
    // IDEA: Add error if `Vec<Resolution>` is empty
    pub fn available_resolutions(&self) -> Result<Vec<Resolution>, String> {
        let outputs: Vec<Output> = self.output_info();

        let resolutions: Vec<Resolution> = outputs.into_iter().map(|o| o.resolution).collect();

        Ok(resolutions)
    }

    /// Get available resolutions for a specific connector in `String` form. If the resolution
    /// is not found it returns an empty `Vec<String>` not an error.
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let available_resolutions = &connector.available_resolutions_pretty()?;
    ///
    ///     println!("Available Resolutions: {:#?}", available_resolutions);
    ///
    /// # assert_eq!(available_resolutions, &[
    /// #     "1920x1080".to_string(),
    /// # ].to_vec());
    ///     Ok(())
    /// }
    /// ```
    // IDEA: Add error if `Vec<String>` is empty
    pub fn available_resolutions_pretty(&self) -> Result<Vec<String>, String> {
        let outputs: Vec<Output> = self.output_info();

        let resolutions: Vec<String> = outputs.into_iter().map(|o| o.resolution.pretty()).collect();

        Ok(resolutions)
    }

    /// Get available refresh rates for a specific connector at a given resolution. If the resolution
    /// is not found it returns an empty `Vec<String>` not an error.
    ///
    /// ## Example
    ///
    /// ```edition2021
    /// #[allow(non_snake_case)]
    ///
    /// use xrandr_parser::Parser;
    ///
    /// fn main() -> Result<(), String> {
    ///     let mut XrandrParser = Parser::new();
    ///
    ///     XrandrParser.parse()?;
    ///
    ///     let connector = &XrandrParser.get_connector("HDMI-1")?;
    ///
    ///     let available_refresh_rates = &connector.available_refresh_rates("1920x1080")?;
    ///
    ///     println!("Available Refresh Rates: {:#?}", available_refresh_rates);
    ///
    /// # assert_eq!(available_refresh_rates, &[
    /// #     "60.00".to_string(),
    /// # ].to_vec());
    ///     Ok(())
    /// }
    /// ```
    // IDEA: Add error if `Vec<String>` is empty
    pub fn available_refresh_rates(&self, target_resolution: &str) -> Result<Vec<String>, String> {
        let mut outputs: Vec<Output> = self.output_info();

        outputs.retain(|o| o.resolution.pretty() == target_resolution);

        Ok(outputs.into_iter().map(|o| o.rates).flatten().collect())
    }
}
