# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Changelog

### Changed

- Updated README.md with installation and usage.

## [0.3.0] - 2022-07-11

### Removed

- Available orientations from disconnected monitors.

### Fixed

- X/Y axis in available orientations would be split at space delimiter.

## [0.2.0] - 2022-07-11

### Added

- Serde Serialize and Deserialize traits to all structures

## [0.1.0] - 2022-07-08

### Added

- Added Parser, Screen, Connector, Position, Dimensions, Resolution and Output structures.
- Implemented getters on parser struct.
- Implemented parse method with associated functions on parser struct.
- Implemented getters and setters on the connector struct.
- Implemented pretty function on the resolution struct to convert resolution to a string in the form of `{horizontal}x{vertical}`.
- Added Examples to methods.
- Added Crate Level Documentation.

[unreleased]: https://gitlab.com/Th3-S1lenc3/xrandr-parser/-/compare/v0.3.0...master
[0.3.0]: https://gitlab.com/Th3-S1lenc3/xrandr-parser/-/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/Th3-S1lenc3/xrandr-parser/-/tags/v0.1.0
[0.1.0]: https://gitlab.com/Th3-S1lenc3/xrandr-parser/-/compare/eb765a27ddf863976012f8448237f5f73c50c35a...v0.1.0
